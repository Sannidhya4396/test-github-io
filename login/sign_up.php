<?php
$showAlert = false;
$showError = false;
$noInput = false;
// $exists == false;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$servername = "localhost";
	$username = "root";
	$password = "";
	$database = "contact";

	// Create a connection
	$conn = mysqli_connect($servername, $username, $password, $database);

	if ($conn) {
		// echo "Connected";
	}
	$name = isset($_POST['username']) ? $_POST['username'] : ' ';
	$pass = isset($_POST['password']) ? $_POST['password'] : ' ';
	$confirm = isset($_POST['cpassword']) ? $_POST['cpassword'] : ' ';
	// $exists == false;
	// checking whether username exists
	$existSql = "SELECT * FROM `nomo` WHERE `users`='$name'";
	$result = mysqli_query($conn, $existSql);
	$numExistRows = mysqli_num_rows($result);
	if ($numExistRows > 0) {
		// $exists = true;
		$showError = "Your account already exists";
	} else {
		// $exists = false;
		if ($pass == NULL) {
			$noInput = True;
		}
		if ($pass == $confirm) /*&& $exists == false)*/ {
			// $exists == false;
			// $sno = +0;g
			$hash = password_hash($pass, PASSWORD_DEFAULT);
			$sql = "INSERT INTO `nomo` ( `users`, `pusers`, `datta`) VALUES ( '$name', '$hash', current_timestamp());";
			// $resultmode = mysqli_store_result();
			// global $sql; 
			// echo $sql;
			$result = mysqli_query($conn, $sql);
			global $result;
			// global	$result;
			// $showAlert = false;
			if ($result) {
				// echo "Success" . mysqli_connect_error();
				$showAlert = true;
			}
			// if (!$result) {
			// echo "Success" . mysqli_connect_error();
			// echo "The record was not inserted successfully because of this error ---> " .  mysqli_error($conn);
			// }
		} else {
			$showError = "Your Passwords do not match";
		}
	}
}


?>

<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

	<title>Sign Up </title>
	<style>
		.god {
			font-size: 15px;
			/* color: red; */
		}
	</style>
</head>

<body>

	<?php
	include '_nav.php';
	?>
	<?php

	// if ($showAlert) {
	// echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
	//   <strong>Congratulations!</strong> You have successfully signed in!
	//   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	// <span aria-hidden="true">&times;</span>
	//   </button>
	// </div>';
	// }
	// if ($showError) {
	// echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
	//   <strong>Error!</strong>  Your passwords do not match!
	//   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	// <span aria-hidden="true">&times;</span>
	//   </button>
	// </div>';
	// }

	?>
	<div class="container my-5">
		<h1 class="text-center">Create New Account</h1>
		<form action="/harry/login/sign_up.php" method="POST">
			<div class="form-group">
				<label for="username">Username</label>
				<input type="text" maxlength="20" class="form-control" id="username" name="username" aria-describedby="emailHelp">
				<?php
				if ($noInput) {
					echo "<small id='emailHelp' class='form-text text-danger '>Please fill out this field!</
			small>";
				}
				?>
			</div>
			<div class="form-group">
				<label for="password">Password</label>
				<input type="password" maxlength="50" class="form-control" id="password" name="password">
			</div>
			<div class="form-group">
				<label for="cpassword">Confirm Password</label>
				<input type="password" maxlength="50" class="form-control" id="cpassword" name="cpassword">
				<?php
				if ($showAlert) {
					echo "<small id='emailHelp' class='form-text text-success god r'>Congratulations! You have Successfully Signed Up!</small>";
				}
				if ($showError) {
					echo "<small id='emailHelp' class='form-text text-danger god r'>Oops! $showError</
		small>";
				}
				?>
			</div>
			<button type="submit" class="btn btn-primary">Sign Up</button>
		</form>
	</div>
	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>

</html>