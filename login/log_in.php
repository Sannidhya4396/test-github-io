<?php


use PhpMyAdmin\SqlParser\Statement;

// structural variables with default values to validate users and thier hashed passwords and also throw designed errors if any.

$login = false;
$showError = false;
// $exists == true; (ignore)

// code written for the condition: If the server request method is post which is validated with isset. This condition or if statement runs only if the $_SERVER REQUEST_METHOD is POST.


////////////////////////////////////////////////////////////


// code written for the condition: if the server request method is post which is validated with the boolean value true (not mentioned). This condition or if statement runs only if the $_SERVER REQUEST_METHOD is POST.


if ($_SERVER["REQUEST_METHOD"] == "POST") {
	// defining the variable
	$servername = "localhost";
	$username = "root";
	$password = "";
	$database = "contact";

	// Create a connection
	$conn = mysqli_connect($servername, $username, $password, $database);
	// checking the connection (ignore after checked. Recheck if you get error)
	if ($conn) {
		// echo "Connected";
	}


	/* defining that $name will take input from the input tags with name as username and pass as password for the inputs Username and Password respectivly. $name is the variable for taking the input of the username and $pass is the variable for taking the input of the Password.
	the isset function is used to ensure that the POST method is set or succsessful.*/


	$name = isset($_POST['username']) ? $_POST['username'] : ' ';
	$pass = isset($_POST['password']) ? $_POST['password'] : ' ';





	// code for debugging

	/*$confirm = isset($_POST['cpassword']) ? $_POST['cpassword'] : ' ';
	 $exists == false;
	 if ($pass == $confirm) {
	 $exists == false;
	 $sno = +0;
	 $sql = "SELECT * FROM `nomo` WHERE `users`='$name' AND  `pusers`='$pass' ";*/


	///////////////////




	// the sql query to be executed is written in the variable sql //

	$sql = "SELECT * FROM `nomo` WHERE `users`='$name' ";


	// code for debugging


	/* echo $num . " records found in the DataBase<br>";
	 $resultmode = mysqli_store_result();
	 global $sql; 
	 echo $sql;*/

	///////////////

	// the $result variable runs and also defines the MySqli query which contains two arguments or parameters which is $conn (the connector) and $sql (the query). The $num variable tells the number of rows in the result syntax or the table connected with.

	$result = mysqli_query($conn, $sql);
	$num = mysqli_num_rows($result);


	// if you echo $num it will show you the number of rows in the DataBase.
	// if you echo $row['name of coloumn'] it will show you the data in the coloumn you mentioned from the DataBase.

	// code for debugging

	/* echo $num . " records found in the DataBase<br>";
	echo  $row['pusers'];
	$num = mysqli_num_rows($result);
	echo $num;
	echo $result;
	global $result;
	global	$result;
	$showAlert = false;*/

	//////////////////////////////////
	/*the if Statement says that if the number of rows in the table is 1 with the name given by the user (it has to be one as we have used unique constraint in PhpMyAdmin) then run the while loop where it will check and fetch the data of $result. The next if Statement configures the password of the user who has logged in. If the passworde matches the hash it allows to proceed or else it proceeds with an error and runs the else statement mentioned below to support it.*/
	if ($num == 1) {
		while ($row = mysqli_fetch_assoc($result)) {
			if (password_verify($pass, $row['pusers'])) {
				$login = true;




				// A session is started here which will keep the user identified and also help in dynamicity and log him/her out. In it the loggedin session is said to be true to help the code be successful executed. The username session is said to be $name to tell compiler and interpreter that the user is the input given. For eg: if you had entered John in the place of the username in the website, this code will take john in the variable $name and put you (John) in the session username. Once all these conditions including those above are true, the code will become successful and take you to the page called welcome.php which is defined and configured with the header function. Welcome.php is anopther page with different code but the same user session. The else statement tells that if the if statement is false or unsuccessful the else will run which will show our designed error message.


				session_start();
				$_SESSION['loggedin'] = true;
				$_SESSION['username'] = $name;
				header("location: welcome.php");
			} else {
				$showError = true;
			}
		}
	}
	/*Code for debugging*/

	// if (!$result) {
	// echo "Success" . mysqli_connect_error();
	// echo "The record was not inserted successfully because of this error ---> " .  mysqli_error($conn);
	// }

	//////////////////////////////
	// this else staement tells that if $num is not equal to 1 the what it should throw. In this case it will make $showError true as above and throw a designed and developer customizable error message


	else {
		$showError = true;
	}
}



/////////////////////////////////////////////////////////////////////////

?>


<!-- Our HTML code starts here -->

<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

	<title>Log In</title>
	<style>
		.god {
			font-size: 15px;
			/* color: red; */
		}
	</style>
</head>

<body>


	<!-- this php include command is used for includng the file _nav.php which contains the code for the navbar and also to keep the code clean. -->
	<?php
	include '_nav.php';
	?>
	<?php


	/*This was pre defined for success and error messages respectively. Further designing has caused fatal changes. Now this code below is dead*/


	// if ($login) {
	// // echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
	// //   <strong>Congratulations!</strong> You have successfully signed in!
	// //   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	// <span aria-hidden="true">&times;</span>
	//   </button>
	// </div>' . mysqli_connect_error();
	// }
	// if ($showError) {
	// echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
	//   <strong>Error!</strong>  Invalid Credentials
	//   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	// <span aria-hidden="true">&times;</span>
	//   </button>
	// </div>';
	// }

	?>

	<!-- the code for the log in set up. This code helps display the front end of the log in bars -->
	<div class="container my-5">
		<h1 class="text-center">Log In to our website </h1>
		<form action="/harry/login/log_in.php" method="POST">
			<div class="form-group">
				<label for="username">Username</label>
				<input type="text" class="form-control" id="username" name="username" aria-describedby="emailHelp">
			</div>
			<div class="form-group">
				<label for="password">Password</label>
				<input type="password" class="form-control" id="password" name="password">
				<?php
				// this line of if Statement ensures that if $login is true which means the code is successful it has to show a success message
				if ($login) {
					echo "<small id='emailHelp' class='form-text text-success text-danger'>Congratulations! You have Successfully Logged In!<small>";
				}
				// this line of if Statement ensures that if $showError is true which means the code is unsuccessful it has to show an error message
				if ($showError) {
					echo "<small id='emailHelp' class='form-text text-success text-danger'>Oops! Your credentials are invaid!<small>";
				}
				?>
			</div>
			<button type="submit" class="btn btn-primary">Log In</button>
		</form>
	</div>
	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>

</html>